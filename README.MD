# TASK MANAGER

## DEVELOPER INFO

* **Name**: Igor Zuev

* **E-mail**: izuev@t1-consulting.ru

* **Phone**: +7 (960) 132-48-73

## SOFTWARE

* **OS**: Windows 10 Pro 21H1

* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: AMD Ryzen 5 3600 6-Core Processor 3.59 GHz

* **RAM**: 32GB

* **SSD**: 512GB

## PROGRAM BUILD

```bash
mvn clean install
```

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```