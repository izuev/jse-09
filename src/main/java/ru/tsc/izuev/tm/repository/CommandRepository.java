package ru.tsc.izuev.tm.repository;

import ru.tsc.izuev.tm.api.ICommandRepository;
import ru.tsc.izuev.tm.constant.ArgumentConst;
import ru.tsc.izuev.tm.constant.CommandConst;
import ru.tsc.izuev.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show command list.");

    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version info.");

    private static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system info.");

    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info.");

    private static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    private static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, INFO, ABOUT, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }
}
