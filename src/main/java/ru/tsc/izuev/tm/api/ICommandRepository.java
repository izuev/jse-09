package ru.tsc.izuev.tm.api;

import ru.tsc.izuev.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
